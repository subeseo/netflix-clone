import Head from "next/head";
import styles from "../styles/Home.module.css";

const categories = [];

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Netflix clone</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div style={{ width: "100%", display: "flex", alignItems: "center" }}>
        <img src="/download.png" style={{ height: "50px" }} />
        <div style={{ marginLeft: "20px" }}>Inicio</div>
        <div style={{ marginLeft: "20px" }}>Series TV</div>
        <div style={{ marginLeft: "20px" }}>Películas</div>
        <div style={{ marginLeft: "20px" }}>Novedades más vistas</div>
      </div>
      <div
        style={{
          width: "100%",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <img src="/breakingbad.jpg" />
        <p
          style={{ marginLeft: "50px", marginTop: "-50px", fontWeight: "bold" }}
        >
          LA MEJOR SERIE DE LA ÚLTIMA DÉCADA
        </p>
      </div>

      <div style={{ width: "100%", padding: "20px" }}>
        <h1>Ciencia ficción</h1>
        <div style={{ width: "100%", display: "flex", alignItems: "center" }}>
          <div style={{ flex: 0.25, padding: "10px" }}>
            <img
              src="/ciencia1.jpg"
              style={{ objectFit: "cover", width: "100%" }}
            />
          </div>
          <div style={{ flex: 0.25, padding: "10px" }}>
            <img
              src="/ciencia2.jpg"
              style={{ objectFit: "cover", width: "100%" }}
            />
          </div>
          <div style={{ flex: 0.25, padding: "10px" }}>
            <img
              src="/ciencia3.jpg"
              style={{ objectFit: "cover", width: "100%" }}
            />
          </div>
          <div style={{ flex: 0.25, padding: "10px" }}>
            <img
              src="/ciencia4.jpg"
              style={{ objectFit: "cover", width: "100%" }}
            />
          </div>
        </div>
      </div>

      <div style={{ width: "100%", padding: "20px" }}>
        <h1>Acción</h1>
        <div style={{ width: "100%", display: "flex", alignItems: "center" }}>
          <div style={{ flex: 0.25, padding: "10px" }}>
            <img
              src="/accion1.jpg"
              style={{ objectFit: "cover", width: "100%" }}
            />
          </div>
          <div style={{ flex: 0.25, padding: "10px" }}>
            <img
              src="/accion2.jpg"
              style={{ objectFit: "cover", width: "100%" }}
            />
          </div>
          <div style={{ flex: 0.25, padding: "10px" }}>
            <img
              src="/accion3.jpg"
              style={{ objectFit: "cover", width: "100%" }}
            />
          </div>
          <div style={{ flex: 0.25, padding: "10px" }}>
            <img
              src="/accion4.jpg"
              style={{ objectFit: "cover", width: "100%" }}
            />
          </div>
        </div>
      </div>

      <div style={{ width: "100%", padding: "20px" }}>
        <h1>Deportes</h1>
        <div style={{ width: "100%", display: "flex", alignItems: "center" }}>
          <div style={{ flex: 0.25, padding: "10px" }}>
            <img
              src="/deportes1.png"
              style={{ objectFit: "cover", width: "100%" }}
            />
          </div>
          <div style={{ flex: 0.25, padding: "10px" }}>
            <img
              src="/deportes2.png"
              style={{ objectFit: "cover", width: "100%" }}
            />
          </div>
          <div style={{ flex: 0.25, padding: "10px" }}>
            <img
              src="/deportes3.png"
              style={{ objectFit: "cover", width: "100%" }}
            />
          </div>
          <div style={{ flex: 0.25, padding: "10px" }}>
            <img
              src="/deportes4.png"
              style={{ objectFit: "cover", width: "100%" }}
            />
          </div>
        </div>
      </div>

      <div style={{ width: "100%", padding: "20px" }}>
        <h1>Documentales</h1>
        <div style={{ width: "100%", display: "flex", alignItems: "center" }}>
          <div style={{ flex: 0.25, padding: "10px" }}>
            <img
              src="/docu1.jpg"
              style={{ objectFit: "cover", width: "100%" }}
            />
          </div>
          <div style={{ flex: 0.25, padding: "10px" }}>
            <img
              src="/docu2.jpg"
              style={{ objectFit: "cover", width: "100%" }}
            />
          </div>
          <div style={{ flex: 0.25, padding: "10px" }}>
            <img
              src="/docu3.png"
              style={{ objectFit: "cover", width: "100%" }}
            />
          </div>
          <div style={{ flex: 0.25, padding: "10px" }}>
            <img
              src="/docu4.jpg"
              style={{ objectFit: "cover", width: "100%" }}
            />
          </div>
        </div>
      </div>
    </div>
  );
}
